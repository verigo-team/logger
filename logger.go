package logger

import (
	"errors"
	"fmt"
	"log"
	"os"
	"runtime"
	"time"
)

var extra = func() string {
	return ``
}

func SetOutput(path string) *os.File {
	logFile, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		Fatal(err, `error opening file for logs`)
	}

	log.SetOutput(logFile)

	extra = func() string {
		return fmt.Sprintf(`time=%v, `, time.Now())
	}

	return logFile
}

// Fatal prints formatted error and exit or ignores if error equals nil
func Fatal(err error, template string, params ...interface{}) {
	if err == nil {
		return
	}

	_, fn, line, _ := runtime.Caller(1)

	msg := fmt.Sprintf(template, params...)

	log.Fatalf("%s[error] [%s] %v %s:%d", extra(), msg, err, fn, line)
}

// Error prints message and error verbose or ignores if error equals nil
func Error(err error, template string, params ...interface{}) {
	if err == nil {
		return
	}

	_, fn, line, _ := runtime.Caller(1)

	msg := fmt.Sprintf(template, params...)

	log.Printf("%s[error] [%s] %v - %s:%d", extra(), msg, err, fn, line)
}

// Info just prints message
func Info(template string, params ...interface{}) {
	_, fn, line, _ := runtime.Caller(1)

	msg := fmt.Sprintf(template, params...)

	log.Printf("%s[info] [%s] - %s:%d", extra(), msg, fn, line)
}

// Stack adds to message new level of error message
func Stack(err error, template string, params ...interface{}) error {
	msg := fmt.Sprintf(template, params...)

	return errors.New(
		fmt.Sprintf(`%s: %s`, msg, err.Error()),
	)
}
