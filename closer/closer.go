package closer

import "bitbucket.org/verigo-team/logger"

// TryClose tries exec close or exit with error message
func TryClose(closeFunc func() error) {
	err := closeFunc()
	logger.Fatal(err, `func was not closed`)
}

