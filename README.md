## It is a tiny library for remove couple of rows golang code

#### How it works:

You can write instead
```go
if err := someFunc(); err != nil {
    log.Fatalf(`...`)
}
```
###### or
```go
err := someFunc()
if err != nil {
	log.Fatalf(`...`)
}
```

just like this 
```go
err := someFunc()
logger.FailOnError(err, `...`)
```

##### Same one for error and info levels